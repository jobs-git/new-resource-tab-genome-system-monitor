# New Resource Tab of Genome System Monitor

**Summary**

A prototype project that will lead to become a new MR for gnome system monitor.

See: https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/206

**Detail**

Next Update on: <2022

See previous works

https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/merge_requests/10 https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/merge_requests/11 https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/98 https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/119 https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/125

Could close

https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/59 https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/issues/70

![system_monitor_resource_1](https://gitlab.gnome.org/GNOME/gnome-system-monitor/uploads/39e83520a21bef2c9520a80859792a8b/system_monitor_resource_1.png)

![system_monitor_resource_2](https://gitlab.gnome.org/GNOME/gnome-system-monitor/uploads/61b0a12515250aa5c09a7b8117ac43d0/system_monitor_resource_2.png)

![system_monitor_resource_3](https://gitlab.gnome.org/GNOME/gnome-system-monitor/uploads/c76c86ec33a43aac6c28daf3c3c921f3/system_monitor_resource_3.png)

Processor View

- Kernel Time (Syscal) - /proc/stat col 3 https://man7.org/linux/man-pages/man5/proc.5.html - in libgtop

- Temp - lm_sensors - needs sudo (alternative? how to get temp without sudo?)

Disk View

- Latency - See MR https://gitlab.gnome.org/GNOME/libgtop/-/merge_requests/27 (/proc/diskstats col 13 https://www.kernel.org/doc/Documentation/ABI/testing/procfs-diskstats)

- Temp - hddtemp - needs sudo? (how gnome-disk gets temp without sudo?)

GPU View (pending libgtop)

- NVIDIA - See MR https://gitlab.gnome.org/GNOME/libgtop/-/merge_requests/30

- AMD - See MR https://gitlab.gnome.org/GNOME/libgtop/-/merge_requests/30

- Intel - reject (intel utility requires sudo, how to get data without using sudo?)

- Other GPU - reject


Other PCI-E device

- N/A
