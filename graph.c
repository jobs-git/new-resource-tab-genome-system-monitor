/* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
//#include <config.h>

#include <math.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <cairo.h>

#include <glibtop.h>
#include <glibtop/cpu.h>
#include <glibtop/mem.h>
#include <glibtop/swap.h>
#include <glibtop/netload.h>
#include <glibtop/netlist.h>
#include <glibtop/disk.h>
#include <glibtop/gpu.h>
#include <glibtop/sysinfo.h>
#include <glibtop/uptime.h>

//#include "application.h"
#include "graph_util.h"
#include "graph.h"


#define PROC_LINE_COLOR
#define RAM_LINE_COLOR
#define SWAP_LINE_COLOR
#define NET_UP_LINE_COLOR
#define NET_D_LINE_COLOR
#define DISK_READ_LINE_COLOR
#define DISK_WRITE_LINE_COLOR
#define GPU_LINE_COLOR

/**
 * 
 * Can only handle less than 1024 of logical hardware per resource.
 * Increase MAX_NGRAPH if society has come to a point where hardware has become too powerfull.
 * Ex. Normal consumer processor might have 1024 logical core from 300 cpu.
 * Kernel might report this as: Max item of 1025 or more.
 * So libgtop reports the same way.
 * 
 * */

void
get_system_info (struct Graph_data_t *graph)
{
	const glibtop_sysinfo * sysinfo = glibtop_get_sysinfo ();

	//model = g_hash_table_lookup (sysinfo->cpuinfo [1].values, "model name");
	//graph->name = g_strdup (model);

	//TO FIX
	///graph->name = sysinfo->model;
}

/* Need a faster way to get ncpu, sysinfo is slow, which the existing libgtop_cpu uses too, so both are slow */
void 
get_proc_info (struct Graph_data_t *graph)
{
	guint i;
	gfloat time;
	graph->stats = LOGICAL;

	const glibtop_sysinfo * sysinfo;

	glibtop_init();
	sysinfo = glibtop_get_sysinfo ();
	glibtop_close ();

	time = 1.0f*g_get_monotonic_time (); /* Time */
	graph->total.ngraph = 1;
	graph->nlogical.ngraph = sysinfo->ncpu;

	prefill_all (graph);

	graph->total.limit [1].min = 1;
	graph->total.limit [1].max = 1;

	graph->nlogical.limit [1].min = 1;
	graph->nlogical.limit [1].max = 1;

	for (i; i < graph->nlogical.ngraph; i++){
		graph->nlogical.temp_data [2][i] = time;
		graph->nlogical.temp_data [4][i] = time;
	}

	get_multi_graph_dim (&graph->nlogical.matrix.x, &graph->nlogical.matrix.y, graph->nlogical.ngraph);
}

void 
get_mem_info (struct Graph_data_t *graph)
{
	gfloat swapusage;
	graph->stats = TOTAL | PAIR;

	glibtop_mem mem;
	glibtop_swap swap;

	glibtop_init();
	glibtop_get_mem (&mem);
	glibtop_get_swap (&swap);
	glibtop_close ();

	/* There's no swap on LiveCD : 0.0f is better than NaN :) */
	swapusage = (swap.total ? swap.total : 0.1f);
	graph->total.ngraph = 1; /* RAM and swap */
	prefill_all (graph);

	/* Set minimum so graph should not appear saturated when idle */
	/* Data Type 1 */
	graph->total.limit [1].min = mem.total; /* RAM */
	graph->total.limit [1].max = mem.total;

	/* Data Type 2 */	
	graph->total.limit [2].min = swapusage; /* swap */
	graph->total.limit [2].max = swapusage;
}

void 
get_net_info (struct Graph_data_t *graph)
{
	graph->stats = TOTAL | PAIR;

	graph->total.ngraph = 1; /* UP and DL*/
	prefill_all (graph);

	/* Set minimum so graph should not appear saturated when idle */
	/* UP & DL */
	graph->total.limit [1].min = 1024*1.0f; /* 1 KiB/s */
	graph->total.limit [1].max = 1024*1.0f; /* 1 KiB/s */
	graph->total.temp_data [3][0] = 1.0*g_get_monotonic_time (); /* Time */
}

void 
get_disk_info (struct Graph_data_t *graph)
{
	graph->stats = LOGICAL | PAIR;

	glibtop_disk disk;

	glibtop_init();
	glibtop_get_disk (&disk);
	glibtop_close ();

	graph->total.ngraph = 1;
	graph->nphysical.ngraph = glibtop_global_server->nreal_disk ? glibtop_global_server->nreal_disk : 1;
	graph->nlogical.ngraph = glibtop_global_server->ndisk ? glibtop_global_server->ndisk : 1;
	prefill_all (graph);

	/* Set minimum so graph should not appear saturated when idle */
	/* Data Type 1: UP */
	graph->total.limit [1].min = 1024*1.0f; /* 1 KiB/s */
	graph->total.limit [1].max = 1024*1.0f; /* 1 KiB/s */

	/* Set minimum so graph should not appear saturated when idle */
	/* Data Type 1: UP */
	graph->nlogical.limit [1].min = 1024*1.0f; /* 100 KiB/s */
	graph->nlogical.limit [1].max = 1024*1.0f; /* 100 KiB/s */	

	get_multi_graph_dim (&graph->nlogical.matrix.x, &graph->nlogical.matrix.y, graph->nlogical.ngraph);
}

/* TODO */
void 
get_gpu_info (struct Graph_data_t *graph)
{
	graph->stats = PHYSICAL;

	glibtop_gpu gpu;

	glibtop_init();
	glibtop_get_gpu (&gpu);
	glibtop_close ();
    
	graph->total.ngraph = 1;
	graph->nphysical.ngraph = glibtop_global_server->ngpu ? glibtop_global_server->ngpu : 1;
	prefill_all (graph);

	get_multi_graph_dim (&graph->nphysical.matrix.x, &graph->nphysical.matrix.y, graph->nphysical.ngraph);
}

void
get_system_uptime (struct Load_data_t *data)
{
	guint sys_days, sys_hrs, sys_mins, sys_secs;

	glibtop_uptime sys_time;

	glibtop_init();
	glibtop_get_uptime (&sys_time);
	glibtop_close ();

	guint time_remains = sys_time.uptime;

	guint day = 3600*24;
	guint hr = 3600;
	guint min = 60;

	sys_days = time_remains / day;
	time_remains %= day;

	sys_hrs = time_remains / hr;
	time_remains %= hr;

	sys_mins = time_remains / min;
	time_remains %= min;

	sys_secs = time_remains;

    data->systime = g_strdup_printf("%02d days %02d hr %02d min %02d s", sys_days, sys_hrs, sys_mins, sys_secs);
}

void
get_graph_info (struct Load_data_t *data) 
{
	get_proc_info (&data->proc);
	get_mem_info (&data->mem);
	get_net_info (&data->net);
	get_disk_info (&data->disk);
	get_gpu_info (&data->gpu);
}



/* 
	Temp Data Structure
	0 - total
	1 - used

	Raw Data Structure
	0 - x coordinate
	1 - load
	2 - kernel time
	3 - temperature
 */
void
get_proc (struct Graph_data_t *graph) 
{
	guint i, j = 1, k, offset = 0, nlogical_data_length;
	gfloat total, used, load, last_x, x_end;
	glibtop_cpu cpu;
	const glibtop_sysinfo * sysinfo;

	glibtop_init();
	glibtop_get_cpu (&cpu);
	sysinfo = glibtop_get_sysinfo ();
	glibtop_close ();

	k = graph->nlogical.matrix.y - 1;
    
	/* If there is a system change, reinitialize everything */
	if (sysinfo->ncpu != graph->nlogical.ngraph) 
	{
		//initialize_resource (graph);
		//get_proc_info (graph);
	}

	nlogical_data_length = graph->nlogical.ngraph*X_RESOLUTION;
	x_end = offset + X_RESOLUTION;

	total = cpu.total*1.0f;
	used = (cpu.user + cpu.nice + cpu.sys)*1.0f;
	load = ((used - graph->total.temp_data [2][0]) / (total - graph->total.temp_data [1][0]));

	graph->total.temp_data [1][0] = total;
	graph->total.temp_data [2][0] = used;

	add_data (&*graph->total.raw [1], load / graph->total.limit [1].max, offset, x_end);

    for (i = 0; i < graph->nlogical.ngraph; i++) 
    {
		offset = i*X_RESOLUTION;
		x_end = offset + X_RESOLUTION;

		total = cpu.xcpu_total [i]*1.0f;
		used = (cpu.xcpu_user[i] + cpu.xcpu_nice[i] + cpu.xcpu_sys[i])*1.0f;
		load = (used - graph->nlogical.temp_data [2][i]) / (total - graph->nlogical.temp_data [1][i]);

		graph->nlogical.temp_data [1][i] = total; /* xproc total */
		graph->nlogical.temp_data [2][i] = used; /* xproc used */

		add_data (&*graph->nlogical.raw [1], load/graph->nlogical.limit [1].max, offset, x_end); /* proc load */
    }

	layout_plot (&graph->nlogical, nlogical_data_length, graph->stats);
	layout_plot (&graph->total, X_RESOLUTION, graph->stats);

	/* Generate cairo grid data */
}

void
get_mem (struct Graph_data_t *graph) 
{
	gfloat rampercent, swappercent, toggle_scalling, ram_factor, swap_factor, factor, max;
	guint i, j = 1, k, offset = 0, data_length;
	gfloat total, used, load, last_x, x_end;
	
	glibtop_mem mem;
	glibtop_swap swap;

	glibtop_init();
	glibtop_get_mem (&mem);
	glibtop_get_swap (&swap);
	glibtop_close ();

	k = graph->total.matrix.y - 1;

	data_length = X_RESOLUTION;
	x_end = offset + X_RESOLUTION;

	/* There's no swap on LiveCD : 0.0f is better than NaN :) */
    swappercent = (swap.total ? (float)swap.used / (float)swap.total : 0.0f);
    rampercent  = 1.0*mem.user / mem.total;

	/* If there is a system change, reset graph */
	if ((int)mem.total != (int)graph->total.limit [1].max || (int)swap.total != (int)graph->total.limit [2].max) 
	{

		//BUG
		//initialize_resource (graph);
		//get_mem_info (graph);
	}

	add_data (&*graph->total.raw [1], rampercent, 0, X_RESOLUTION);	/* ram data*/
	add_data (&*graph->total.raw [2], swappercent, 0, X_RESOLUTION); /* swap data */

	layout_plot (&graph->total, X_RESOLUTION, graph->stats);
}

/* 
	Temp Data Structure
	0 - in bytes
	1 - out bytes

	Raw Data Structure
	0 - x coordinate
	1 - in bandwidth + out bandwidth
 */

/* set global data_limit */
void
get_net (struct Graph_data_t *graph) 
{
	guint i, j = 1, k, offset = 0, data_length;
	gfloat last_x, x_end;

	k = graph->total.matrix.y - 1;

	data_length = X_RESOLUTION;
	x_end = offset + X_RESOLUTION;

   	glibtop_netlist netlist;
    char **ifnames;
    guint64 in = 0, out = 0;
    guint64 time;
    gfloat din, dout;
    gboolean first = TRUE;

	glibtop_init ();

    ifnames = glibtop_get_netlist(&netlist);

    for (i = 0; i < netlist.number; ++i)
    {
        glibtop_netload netload;
        glibtop_get_netload (&netload, ifnames[i]);

        if (netload.if_flags & (1 << GLIBTOP_IF_FLAGS_LOOPBACK))
            continue;

        /* Skip interfaces without any IPv4/IPv6 address (or
           those with only a LINK ipv6 addr) However we need to
           be able to exclude these while still keeping the
           value so when they get online (with NetworkManager
           for example) we don't get a suddent peak.  Once we're
           able to get this, ignoring down interfaces will be
           possible too.  */
        if (!(netload.flags & (1 << GLIBTOP_NETLOAD_ADDRESS6) && netload.scope6 != GLIBTOP_IF_IN6_SCOPE_LINK) && !(netload.flags & (1 << GLIBTOP_NETLOAD_ADDRESS)))
            continue;

        /* Don't skip interfaces that are down (GLIBTOP_IF_FLAGS_UP)
           to avoid spikes when they are brought up */

        in  += netload.bytes_in;
        out += netload.bytes_out;
    }

	glibtop_close ();

    g_strfreev(ifnames);

    time = 1.0f*g_get_monotonic_time ();

    if (in >= graph->total.temp_data [1][0] && out >= graph->total.temp_data [2][0] && graph->total.temp_data [3][0] != 0 && time - graph->total.temp_data [3][0] > 10) {
        float dtime;
        dtime = ((double) (time - graph->total.temp_data [3][0])) / G_USEC_PER_SEC;
        din   = (double)((in  - graph->total.temp_data [1][0])  / dtime);
        dout  = (double)((out - graph->total.temp_data [2][0]) / dtime);
    } else {
        /* Don't calc anything if new data is less than old (interface
           removed, counters reset, ...) or if it is the first time */
        din  = 0;
        dout = 0;
    }

		//	read_speed = isnormal(read_speed) ? read_speed : 0.0f;
		//write_speed = isnormal(write_speed) ? write_speed : 0.0f;

    first = first && (graph->total.temp_data [3][0] == 0);

	graph->total.temp_data [1][0] = 1.0f*in; /* Total bytes recieved */
	graph->total.temp_data [2][0] = 1.0f*out; /* Total bytes sent */
	graph->total.temp_data [3][0] = 1.0f*time; /* Time since */

	rescale_max (&*graph->total.raw [1], 
					data_length, 
					din, 
					0,
					graph->total.limit [1].min,
					&graph->total.limit [1].max, 
					&graph->total.limit [1].max_life,
					1.1f);

	/* offset in life */
	graph->total.limit [1].max_life++;

	rescale_max (&*graph->total.raw [2], 
					data_length, 
					dout, 
					0,
					graph->total.limit [1].min,
					&graph->total.limit [1].max, 
					&graph->total.limit [1].max_life,
					1.1f);

	add_data (&*graph->total.raw [1], din/graph->total.limit [1].max, 0, X_RESOLUTION);	/* Net DL data*/
	add_data (&*graph->total.raw [2], dout/graph->total.limit [1].max, 0, X_RESOLUTION); /* Net UP data */

	layout_plot (&graph->total, X_RESOLUTION, graph->stats);
}

void
get_disk (struct Graph_data_t *graph) 
{
	guint i, j = 1, k, offset = 0, nlogical_data_length;
	gfloat last_x, x_end;
	glibtop_disk disk;
    gfloat time = 0, read = 0, write = 0, time_read = 0, time_write = 0;
	gfloat read_speed = 0, write_speed = 0, total_read_speed = 0, total_write_speed = 0;

	glibtop_init ();
	glibtop_get_disk (&disk);
	glibtop_close ();

	guint nreal_disk_item = glibtop_global_server->nreal_disk ? glibtop_global_server->nreal_disk : 1;
	guint ndisk_item = glibtop_global_server->ndisk ? glibtop_global_server->ndisk : 1;

	k = graph->nlogical.matrix.y - 1;
    
	/* If there is a system change, reinitialize everything */
	if (ndisk_item != graph->nphysical.ngraph || nreal_disk_item != graph->nphysical.ngraph) 
	{
		//initialize_resource (graph);
		//get_disk_info (graph);
	}

	nlogical_data_length = graph->nlogical.ngraph*X_RESOLUTION;

	x_end = offset + X_RESOLUTION;

	for (i = 0; i < graph->nlogical.ngraph; i++) 
    {
		offset = i*X_RESOLUTION;
		x_end = offset + X_RESOLUTION;

		time = 1.0f*g_get_monotonic_time ();
		read = 1.0f*disk.xdisk_sectors_read [i] - graph->nlogical.temp_data [1][i];
		time_read = (time - graph->nlogical.temp_data [2][i])/G_USEC_PER_SEC;
		write = 1.0f*disk.xdisk_sectors_write [i] - graph->nlogical.temp_data [3][i];
		time_write = (time - graph->nlogical.temp_data [4][i])/G_USEC_PER_SEC;

		graph->nlogical.temp_data [1][i] = 1.0f*disk.xdisk_sectors_read [i];
		graph->nlogical.temp_data [2][i] = time;
		graph->nlogical.temp_data [3][i] = 1.0f*disk.xdisk_sectors_write [i];
		graph->nlogical.temp_data [4][i] = time;

		read_speed = read * 512 / time_read; /* 512 - convert sectors to bytes. */
		write_speed = write * 512 / time_write; /* 512 - convert sectors to bytes. */

		read_speed = isnormal(read_speed) ? read_speed : 0.0f;
		write_speed = isnormal(write_speed) ? write_speed : 0.0f;

		total_read_speed += read_speed;
		total_write_speed += write_speed;

		rescale_max (&*graph->nlogical.raw  [1], 
						nlogical_data_length, 
						read_speed, 
						0,
						graph->nlogical.limit [1].min,
						&graph->nlogical.limit [1].max, 
						&graph->nlogical.limit [1].max_life,
						1.1f);

		graph->nlogical.limit [1].max_life++;

		rescale_max (&*graph->nlogical.raw [2], 
						nlogical_data_length, 
						write_speed, 
						0,
						graph->nlogical.limit [1].min,
						&graph->nlogical.limit [1].max, 
						&graph->nlogical.limit [1].max_life,
						1.1f);

		add_data (&*graph->nlogical.raw [1], read_speed/graph->nlogical.limit [1].max, offset, x_end); /* Write speed */
		add_data (&*graph->nlogical.raw [2], write_speed/graph->nlogical.limit [1].max, offset, x_end); /* Write speed */

    }

	/* If total, fit all data in one data row */
	rescale_max (&*graph->total.raw [1], 
					X_RESOLUTION, 
					total_read_speed, 
					0,
					graph->total.limit [1].min,
					&graph->total.limit [1].max, 
					&graph->total.limit [1].max_life,
					1.1f);

	graph->total.limit [1].max_life++;

	rescale_max (&*graph->total.raw [2], 
					X_RESOLUTION, 
					total_write_speed, 
					0,
					graph->total.limit [1].min,
					&graph->total.limit [1].max, 
					&graph->total.limit [1].max_life,
					1.1f);

	add_data (&*graph->total.raw [1], total_read_speed/graph->total.limit [1].max, 0, X_RESOLUTION);
	add_data (&*graph->total.raw [2], total_write_speed/graph->total.limit [1].max, 0, X_RESOLUTION);

	layout_plot (&graph->nlogical, nlogical_data_length, graph->stats);
	layout_plot (&graph->total, X_RESOLUTION, graph->stats);
}

void
get_gpu (struct Graph_data_t *graph) 
{
	guint i, j = 1, k, offset = 0, nphysical_data_length;
	gfloat last_x, x_end;
	glibtop_gpu gpu;
	gfloat total_used = 0, total_vram_used = 0, total_vram;

	glibtop_init ();
	glibtop_get_gpu (&gpu);
	glibtop_close ();

	guint nphysical_gpu_item = glibtop_global_server->ngpu ? glibtop_global_server->ngpu : 1;

	k = graph->nphysical.matrix.y - 1;
    
	/* If there is a system change, reinitialize everything */
	if (nphysical_gpu_item != graph->nphysical.ngraph) 
	{
		initialize_resource (graph);
		get_gpu_info (graph);
	}

	nphysical_data_length = graph->nphysical.ngraph*X_RESOLUTION;

	x_end = offset + X_RESOLUTION;

	for (i = 0; i < graph->nphysical.ngraph; i++) 
    {
		offset = i*X_RESOLUTION;
		x_end = offset + X_RESOLUTION;

		add_data (&*graph->nphysical.raw [1], gpu.xgpu_used_percent [i]/100.0, offset, x_end); /* Write speed */
		add_data (&*graph->nphysical.raw [2], 1.0f*gpu.xgpu_vram_used [i]/gpu.xgpu_vram_total [i], offset, x_end); /* Write speed */
		total_used += gpu.xgpu_used_percent [i];
		total_vram_used += gpu.xgpu_vram_used [i];
		total_vram += gpu.xgpu_vram_total [i];
    }

	add_data (&*graph->total.raw [1], total_used/(100.0*graph->nphysical.ngraph), 0, X_RESOLUTION);
	add_data (&*graph->total.raw [2], 1.0f*total_vram_used/total_vram, 0, X_RESOLUTION);

	/* Generate cairo drawing data*/
	/* modulo of 0 is undefined so shift data in the for loop but recover the index in layout_coord */

	layout_plot (&graph->nphysical, nphysical_data_length, graph->stats);
	layout_plot (&graph->total, X_RESOLUTION, graph->stats);
}

void
transform_to_cartesian (cairo_t *cr, gfloat width, gfloat height)
{
	/* Save invariant detail */
	cairo_set_line_width (cr, 1);
    cairo_save (cr);

    /* Save invariant detail */
    cairo_matrix_t x_reflection_matrix; 
    cairo_matrix_init_identity(&x_reflection_matrix);

    /* reflection through the x axis equals the identity matrix with the bottom 
    left value negated  */
    x_reflection_matrix.yy = -1.0;
    cairo_set_matrix(cr, &x_reflection_matrix);

    /* This would result in your drawing being done on top of the destination 
    surface, so we translate the surface down the full height */
    cairo_translate(cr, 0, -height); // replace SURFACE_HEIGHT

    cairo_scale (cr, width, height);
}

void
cairo_graph_set (struct Resource_data_t *data, cairo_t *cr, gfloat width, gfloat height, guint stats)
{
	transform_to_cartesian (cr, width, height);

	guint i;
	guint length = data->ngraph*X_RESOLUTION;
	guint move = X_RESOLUTION*data->matrix.x;

	/* Do all drawing here */

	cairo_move_to (cr, data->prep [0][0], data->prep [stats][0]);

    for (i = 1; i < length; i++)
    {
		if (i%(move) == 0) 
		{
			/* close the right edge */
			cairo_line_to (cr, 1, data->prep [stats][i]);

			/* x = -1 hack so lines wont cross across graphs*/
			cairo_move_to (cr, -1, data->prep [stats][i]);
		} 
		else
		{
        	cairo_line_to (cr, data->prep [0][i], data->prep [stats][i]);
		}
	
    }

    /* Restore the previously set detail, if we dont do this, line will be scaled wrongly due to cairo_scale */
    cairo_restore (cr);

	/* change line color here */

	cairo_set_source_rgb (cr, 0, 0, 1);

	/* Plot everything */
    cairo_stroke (cr);

	cairo_save(cr);
}

/* Dont calculate anything here. Just draw */
gboolean
draw (GtkWidget *da, cairo_t *cr, gpointer ptr)
{
    struct Load_data_t *data = ptr;
	struct Graph_data_t *graph;
	struct Resource_data_t *res;

    guint i, j, w, h;
	gfloat width, height;

    GtkWidget* widget = gtk_widget_get_parent (da);

	w = gtk_widget_get_allocated_width (widget);
  	h = gtk_widget_get_allocated_height (widget);
    
	//gtk_window_get_size(GTK_WIDGET(window), &w, &h);

	width = w*1.0f;
	height = h*1.0f;

	switch (data->resource_view)
	{
		case PROC:
			graph = &data->proc;
			break;
		case MEM:
			graph = &data->mem;
			break;
		case NET:
			graph = &data->net;
			break;
		case DISK:
			graph = &data->disk;
			break;
		case GPU:
			graph = &data->gpu;
			break;
	}

	if (graph->stats & LOGICAL)
	{
		res = &graph->nlogical;
	}
	else if (graph->stats & PHYSICAL) 
	{
		res = &graph->nphysical;
	}
	else 
	{
		res = &graph->total;
	}

	cairo_graph_set (res, cr, width, height, 1);

	if (graph->stats & PAIR) 
	{
		cairo_new_path(cr);
		cairo_graph_set (res, cr, width, height, 2);
	}

	if (graph->stats & LATENCY) 
	{
		cairo_new_path(cr);
		cairo_graph_set (res, cr, width, height, 3);
	}

	if (graph->stats & TEMP) 
	{
		cairo_new_path(cr);
		cairo_graph_set (res, cr, width, height, 4);
	}

    gtk_widget_queue_draw(da);
    return FALSE;
}

void
graph_update (struct Load_data_t *data) 
{
	get_proc (&data->proc);
	get_mem (&data->mem);


	get_net (&data->net);
	get_gpu (&data->gpu);

	get_disk (&data->disk);	
}

void
transfer_plot (struct Graph_data_t *graph)
{
	guint logical_length = graph->nlogical.ngraph*X_RESOLUTION;
	guint physical_length = graph->nphysical.ngraph*X_RESOLUTION;

	cairo_data (&graph->nlogical, logical_length, graph->stats);
	cairo_data (&graph->nphysical, physical_length, graph->stats); 
	cairo_data (&graph->total, X_RESOLUTION, graph->stats); 
}

void
transfer_data_to_draw (struct Load_data_t *data) 
{
	transfer_plot (&data->proc);
	transfer_plot (&data->mem);
	transfer_plot (&data->net);
	transfer_plot (&data->disk);	
	transfer_plot (&data->gpu);
}

void
get_data (struct Load_data_t *data) 
{
    graph_update (data);
	//transfer_data_to_draw (data);
}

static gboolean
calc_cb (gpointer ptr) 
{
	struct Load_data_t *data = (struct Load_data_t *)ptr;
	get_data (data);
	return TRUE;
}

static void*
calc_thread (gpointer ptr)
{
    struct Load_data_t *data = (struct Load_data_t *)ptr;

    g_timeout_add(1000, (GSourceFunc)calc_cb, data);

	return 0;
}

void
initialize_graph_resource (struct Load_data_t *data) {

	initialize_resource (&data->proc);
}

void
initialize_graph (struct Load_data_t *data) 
{
	/* Do once so we dont get empty screen when calc thread has not yet populated the data */

	initialize_graph_resource (data);

	get_graph_info (data);

    get_data (data);

    /* Launch calculation thread */
	pthread_mutex_init (&data->mutex, NULL);
	pthread_create (&data->calculate_thread, NULL, calc_thread, data);
}

void
load_graph_start (struct Load_data_t *data) 
{
    data->draw = TRUE;
}

void
load_graph_stop (struct Load_data_t *data) 
{
	/* don't draw anymore, but continue to poll */
    data->draw = FALSE;
}