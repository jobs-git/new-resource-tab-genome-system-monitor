/* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef _GSM_GRAPH_H_
#define _GSM_GRAPH_H_

#include <glib.h>
#include <gtk/gtk.h>

void
initialize_graph (struct Load_data_t *data);

gboolean
draw (GtkWidget *da, cairo_t *cr, gpointer ptr);


#endif /* _GSM_GRAPH_H_ */
