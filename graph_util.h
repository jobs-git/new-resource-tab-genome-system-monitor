/* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef _GSM_GRAPH_UTIL_H_
#define _GSM_GRAPH_UTIL_H_

#include <glib.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <libhandy-1/handy.h>

#define X_RESOLUTION 101 /* Cairo drawing image have 101 data points from 0 to 100 */
#define X_DATA_COL 10 /* Starting from 0 */
#define MAX_NGRAPH 1024

enum Graph_stats_e 
{
	/* Data view*/
	TOTAL = 1 << 0,
	NODE = 1 << 1,
	PHYSICAL = 1 << 2,
	LOGICAL = 1 << 3,
	/* Some data is a pair in one graph */
	PAIR = 1 << 4,
	/* Additional data stats */
	LATENCY = 1 << 5,
	TEMP = 1 << 6
};

enum Resource_view_e 
{
	PROC, MEM, NET, DISK, GPU
};



struct Limit_data_t
{
	gfloat min, max;
	//can become negative
	gint max_life;
};

struct Graph_matrix_t
{
	guint x, y;
};

/*
	temp_data
	invariant order

	raw/graph data structure
	0 - x data												color plot 0
	1 - ram		up		read		cpu load	gpu load	color plot 1
	2 - swap	DL		write					ram			color plot 2
	3 - 				latency		latency					color plot 3
	4 -					temp		temp		temp		color plot 4
*/
struct Resource_data_t 
{
	guint ngraph;
	gchar name [X_DATA_COL][MAX_NGRAPH][256];
	gfloat temp_data [X_DATA_COL][MAX_NGRAPH]; /* Data that does not need history but its previous instance is needed to calculate the historical data */
	gfloat color [X_DATA_COL][3];
	
	struct Graph_matrix_t matrix;
	struct Limit_data_t limit [X_DATA_COL];
	
	/* Historical data */
	gfloat raw [X_DATA_COL][X_RESOLUTION*MAX_NGRAPH]; /* Fits one Cairo drawing to max: x + y + others (temperature, kernel time, disk latency, etc) */
	
	/* Prepare data */
	gfloat prep [X_DATA_COL][MAX_NGRAPH*X_RESOLUTION];

	/* Render data */
	/* Fits all graphs in one Cairo drawing: by x by y. Loop only until X_RESOLUTION*(ngraph - not needed) */
	gfloat cairo [X_DATA_COL][MAX_NGRAPH*X_RESOLUTION];
};

struct Graph_data_t 
{
	guint stats;
	
	gchar name [256];
	guint plot_color [X_DATA_COL][3];

	/* Historical data */
	struct Resource_data_t total;
	struct Resource_data_t nphysical;
	struct Resource_data_t nlogical;

	/* Cairo */
	cairo_t *detailed_graph;
	cairo_t *panel_graph;

	GtkWidget *detailed_da;
	GtkWidget *panel_da;
};

struct Load_data_t 
{
	enum Resource_view_e resource_view;
	guint index;
	gboolean draw;
	gchar *systime;
	struct Graph_data_t gpu, proc, mem, net, disk;

	/* Widgets */
	HdyApplicationWindow *main_window;

	/* Needed for calc thread */
    pthread_mutex_t mutex;
	pthread_t calculate_thread;
};

void
get_multi_graph_dim (guint *x, guint *y, guint n);

void
initialize_clear (struct Graph_data_t *graph);

void
add_data (gfloat *arr, gfloat data, guint offset, guint x_end);

void
layout_plot (struct Resource_data_t *data, guint length, guint stats);

void
rescale_max (gfloat *arr, guint lenght, gfloat data, guint offset, gfloat min, gfloat *max, gint *max_life, gfloat overflow_factor);

void
cairo_data (struct Resource_data_t *data, guint length, guint stats);

void
initialize_resource (struct Graph_data_t *graph);

void
prefill_all (struct Graph_data_t *graph) ;

#endif /* _GSM_GRAPH_UTIL_H_ */