//gcc `pkg-config --cflags --libs gtk+-3.0` -lm -lpthread -lgtop-2.0 struct2.c graph.cpp graph_util.cpp `pkg-config --libs glib-2.0` -I./ -I/usr/include/libgtop-2.0 -o struct2 && ./struct2


#include <gtk/gtk.h>
#include <libhandy-1/handy.h>
#include <cairo.h>
#include <glib.h>
#include <math.h>
#include <pthread.h>

#include "graph_util.h"
#include "graph.h"
#include "resource_interface.h"



struct Resource_interface_t
{
    struct Load_data_t *load_data;
    guint left_panel_state;
    guint right_panel_state;
};


void
set_gui (struct Load_data_t *data, GtkBuilder *builder)
{
    GtkWidget *box;
    GtkWidget *darea;
    //darea = gtk_drawing_area_new();
    //gtk_widget_set_size_request(darea, 100, 100);
    darea = GTK_WIDGET (gtk_builder_get_object (builder, "resource_processor_graph"));
    //gtk_container_add(GTK_BOX(box), darea);
    //gtk_box_append (box,darea);
    //gtk_box_pack_start (GTK_BOX(box), darea, FALSE, FALSE, 0);
    //gtk_widget_show (darea);

    g_signal_connect(G_OBJECT(darea), "draw", G_CALLBACK(draw), data);
}



void
start_resource_interface (HdyApplicationWindow *main_window, GtkBuilder *builder)
{
    struct Load_data_t *data;
    
    data = calloc(1, sizeof(struct Load_data_t));
    data->main_window = main_window;

    data->resource_view = PROC;

    initialize_graph (data);
    set_gui (data, builder);
}