/* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef _GSM_RESOURCE_INTERFACE_H_
#define _GSM_RESOURCE_INTERFACE_H_

#include <glib.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <libhandy-1/handy.h>

void
start_resource_interface (HdyApplicationWindow *main_window, GtkBuilder *builder);


#endif /* _GSM_RESOURCE_INTERFACE_H_ */